import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import {Dexie} from 'dexie';


import { Employee } from './employee.model';
import {OnlineOfflineSyncService} from './online-offline-sync.service';

this.newEmp = new Employee();

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  selectedEmployee: Employee;
  employees: Employee[];
  readonly baseURL="http://localhost:3000/employees";

  constructor(private http:HttpClient, private onlineOfflineService:OnlineOfflineSyncService) {
    this.createIndexedDatabase();
    this.registerToEvent(onlineOfflineService);
    this.listenToEvents(onlineOfflineService);
   }

   private registerToEvent(onlineOfflineService:OnlineOfflineSyncService){
     onlineOfflineService.connectionChanged.subscribe(online=>{
       if(online){
         console.log('went online');
         console.log('sending all stored items');

         //this.sendItemsFromIndexedDb();
       }else {
        console.log('went offline, storing in indexdb');
      }
     })
   }

   private listenToEvents(onlineOfflineService){
     onlineOfflineService.connectionChanged.subscribe(online=>{
       if(online){
         console.log('Went online');
        // this.sendItemsToDelete();
       }
       else{
         console.log('went offline');
       }
     })
   }



//   postEmployee(emp:Employee){
// return this.http.post(this.baseURL,emp);
//   }

postEmployee(emp:Employee){
  return this.http.post(this.baseURL,emp);
    }

  getEmployeeList(){
    return this.http.get(this.baseURL);
  }
       
  putEmployee(emp: Employee) {
    return this.http.put(this.baseURL + `/${emp._id}`, emp);
  }

  deleteEmployee(_id: string) {
    return this.http.delete(this.baseURL + `/${_id}`);
  }

  private db:any;
  private donedb:any;

//create Indexed db
 createIndexedDatabase(){
  this.db=new Dexie("TestDatabase");
this.db.version(1).stores({
  newEmp:"_id,name,position,office,salary"
});
this.donedb.open().catch(function(err){
  console.log(err.stack || err);
});
 }

private createDoneDatabase(){
  this.donedb=new Dexie("DoneDatabase");
this.donedb.version(1).stores({
  emp:'_id'
});
this.donedb.open().catch(function(err){
console.log(err.stack || err);
})

}


}