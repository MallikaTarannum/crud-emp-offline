import { TestBed } from '@angular/core/testing';

import { OnlineOfflineSyncService } from './online-offline-sync.service';

describe('OnlineOfflineSyncService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OnlineOfflineSyncService = TestBed.get(OnlineOfflineSyncService);
    expect(service).toBeTruthy();
  });
});
